///use_item_in_inventory(slot)

var slotno = argument0;
var count = obj_inv_controller.inventoryCount[slotno];
count--;
obj_inv_controller.inventoryCount[slotno] = count;
if(count <= 0)
{
    obj_inv_controller.inventory[slotno] = 0;   
    obj_inv_controller.inventoryCount[slotno] = 0;   
}

