///pull_towards_black_holes();

var distance = 0;

with(id)
{
    distance = distance_to_object(obj_black_hole);
    
    if(distance < 250)
    {
        var blackInstance = instance_nearest(x,y,obj_black_hole);
        
        var blackHoleX = blackInstance.x;
        var blackHoleY = blackInstance.y;
        // Determine amount of gravity
        var blackHoleStrength = 1.0;
        
        if(distance > 200)
        {
            blackHoleStrength = 1.0;
        }
        if(distance > 150)
        {
            blackHoleStrength = 2.0;
        }
        else if(distance > 100)
        {
            blackHoleStrength = 3.0;
        }
        else if(distance < 50)
        {
            blackHoleStrength = 4.0;
        }
        gravity_direction = point_direction(x,y,blackHoleX, blackHoleY);
        gravity = blackHoleStrength;
    }
    else
    {
        gravity = 0;
        gravity_direction = 0;
    }
}

