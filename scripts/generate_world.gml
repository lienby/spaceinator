///Generate World


var startx = view_xview[0];
var starty = view_yview[0]
var xmax = startx + view_wview[0];
var ymax = starty + view_hview[0];

xmax = xmax - (xmax % 32);
ymax = ymax - (ymax % 32);

startx = startx - (startx % 32);
starty = starty - (starty % 32);


for(var xx = startx; xx < xmax; xx+=32)
{
    for(var yy = starty; yy < ymax;yy+=32)
    {   
        if(place_empty(xx,yy))
        {           
            instance_create(xx,yy,obj_air);
            
            if(random_range(0,10) > 5)
            {
                instance_create(xx,yy,obj_asteriod);
            }
        }
    }
}


