///add_item_to_inventory(itemId)
var itemId = argument0;

for(var i = 0; i < (9*4); i ++)
{
    var itm = obj_inv_controller.inventory[i];
    
    if(itm == 0)
    {
        obj_inv_controller.inventory[i] = itemId;
        obj_inv_controller.inventoryCount[i] = 1;
        return true;
    }
    else if(itm == itemId)
    {
        obj_inv_controller.inventoryCount[i] ++;
        return true;
    }
}
return false;

