///get_chunk_coords(x,y)
var xx = argument0;
var yy = argument1;

var chunk_total_size = (CHUNK_SIZE*TILE_SIZE);

var playX = xx;
var playY = yy;

var chunkRelX = playX % chunk_total_size;
var chunkRelY = playY % chunk_total_size;

var chunkX = (playX - chunkRelX);
var chunkY = (playY - chunkRelY);

// Fix annoyances where % negative = positive

if(chunkX < 0)
{
    chunkX -= chunk_total_size;
}

if(chunkY < 0)
{
    chunkY -= chunk_total_size;
}

if(chunkY == 0 && (playY < 0 && playY >= chunk_total_size/-1) )
{
    chunkY = chunk_total_size/-1;
}

if(chunkX == 0 && (playX < 0 && playX >= chunk_total_size/-1) )
{
    chunkX = chunk_total_size/-1;
}

var coords = array_create(1);
coords[0] = chunkX;
coords[1] = chunkY;

return coords;
